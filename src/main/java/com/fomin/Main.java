package com.fomin;

public class Main {
    public static void main(String[] args) {
        SimpleBinaryTreeMap<Integer, Integer> theTree = new SimpleBinaryTreeMap<>(64, 78);
        theTree.put(27, 35);
        theTree.put(67, 73);
        theTree.put(59, 19);
        for (var t : theTree.entrySet()) {
            System.out.println(t.getKey() + " " + t.getValue());
        }
        theTree.remove(67);
        theTree.remove(19);
        System.out.println("-----");
        for (var t : theTree.entrySet()) {
            System.out.println(t.getKey() + " " + t.getValue());
        }
        System.out.println(theTree.size());
        System.out.println(theTree.isEmpty());
    }
}