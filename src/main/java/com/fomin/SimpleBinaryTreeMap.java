package com.fomin;

import java.util.*;

public class SimpleBinaryTreeMap<K extends Comparable<K>, V> implements Map<K, V> {
    private int size = 0;
    private Node<K, V> root;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    static class Node<K, V> implements Map.Entry<K, V> {
        private Node<K, V> left;
        private Node<K, V> right;
        private K key;
        private V value;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
    }

    @Override
    public void clear() {
        size = 0;
        root = null;
    }

    @Override
    public V put(K key, V value) {
        Node<K, V> parent = root;
        Node<K, V> tempParent = root;
        if (root == null) {
            root = new Node<>(key, value);
        } else {
            int cmp;
            do {
                parent = tempParent;
                cmp = key.compareTo(tempParent.key);
                if (cmp < 0) {
                    tempParent = tempParent.left;
                } else if (cmp > 0) {
                    tempParent = tempParent.right;
                } else {
                    V oldValue = tempParent.value;
                    tempParent.value = value;
                    return oldValue;
                }
            } while (tempParent != null);

            if (cmp < 0) {
                parent.left = new Node<>(key, value);
            } else if (cmp > 0) {
                parent.right = new Node<>(key, value);
            }
        }
        size++;
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> set = new HashSet<>();
        runTreeForNode(root, set);
        return set;
    }
    private void runTreeForNode(Node<K, V> root, Set<Entry<K, V>> set) {
        if (root != null) {
            runTreeForNode(root.left, set);
            set.add(root);
            runTreeForNode(root.right, set);
        }
    }

    @Override
    public Set<K> keySet() {
        Set<K> keys = new HashSet<>();
        runTreeForKey(root, keys);
        return keys;
    }

    private void runTreeForKey (Node<K, V> root, Set<K> keySet) {
        if (root != null) {
            runTreeForKey(root.left, keySet);
            keySet.add(root.getKey());
            runTreeForKey(root.right, keySet);
        }
    }

    private void runTreeForValue(Node<K, V> localRoot, Collection<V> values) {
        if (localRoot != null) {
            runTreeForValue(localRoot.left, values);
            values.add(localRoot.getValue());
            runTreeForValue(localRoot.right, values);
        }
    }

    @Override
    public Collection<V> values() {
        Collection<V> collection = new ArrayList<>();
        runTreeForValue(root, collection);
        return collection;
    }

    @Override
    public V remove(Object key) {
        K k = (K) key;
        Node<K, V> removeNode = root;
        Node<K, V> parent = root;
        Node<K, V> parentChild = null;
        Node<K, V> child;
        if (root == null) {
            return null;
        } else {
            int compareResult;
            while (true) {
                compareResult = k.compareTo(removeNode.key);
                if (compareResult < 0) {
                    if (removeNode.left == null) {
                        return null;
                    }
                    parent = removeNode;
                    removeNode = removeNode.left;
                } else if (compareResult > 0) {
                    if (removeNode.right == null) {
                        return null;
                    }
                    parent = removeNode;
                    removeNode = removeNode.right;
                } else {
                    break;
                }
            }
            if (removeNode.left == null && removeNode.right == null) {
                if (root == removeNode) {
                    root = null;
                } else if (parent.left == removeNode) {
                    parent.left = null;
                } else if (parent.right == removeNode) {
                    parent.right = null;
                }
            } else if (removeNode.left == null) {
                if (parent.left == removeNode) {
                    parent.left = removeNode.right;
                } else {
                    parent.right = removeNode.right;
                }
            } else if (removeNode.right == null) {
                if (parent.left == removeNode) {
                    parent.left = removeNode.left;
                } else {
                    parent.right = removeNode.left;
                }
            } else {
                child = removeNode.right;
                while (child.left != null) {
                    parentChild = child;
                    child = child.left;
                }
                if (parentChild != null) {
                    parentChild.left = child.right;
                } else {
                    removeNode.right = null;
                }
                child.left = removeNode.left;
                child.right = removeNode.right;
                if (removeNode != root) {
                    if (parent.left == removeNode) {
                        parent.left = child;
                    } else {
                        parent.right = child;
                    }
                } else {
                    root = child;
                }
            }
            size--;
            return removeNode.getValue();
        }
    }





    public SimpleBinaryTreeMap(K key, V value) {
        root = new Node<>(key, value);
        size++;
    }

    @Override
    public V get(Object key) {
        Comparable<? super K> k = (Comparable<? super K>) key;
        Node<K, V> node = root;
        while (node != null) {
            int compareResult = k.compareTo(node.key);
            if (compareResult < 0) {
                node = node.left;
            } else if (compareResult > 0) {
                node = node.right;
            } else {
                return node.value;
            }
        }
        return null;
    }

    private Node<K, V> addRecursive(Node<K, V> node, K key, V value) {
        if (node == null) {
            node = new Node<>(key, value);
            return node;
        }

        int compareResult = key.compareTo(node.key);

        if (compareResult < 0) {
            node.left = addRecursive(node.left, key, value);
        } else if (compareResult > 0) {
            node.right = addRecursive(node.right, key, value);
        } else {
            return node;
        }
        return node;
    }
}
